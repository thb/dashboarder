import { AstNode, Reference, ValidationAcceptor, ValidationCheck, ValidationRegistry } from 'langium';
import { DashboarderAstType, isChartPanel, isComparisonPanel, Page, Panel } from './generated/ast';
import { DashboarderServices } from './dashboarder-module';
import _ from 'lodash';

/**
 * Map AST node types to validation checks.
 */
type DashboarderChecks = { [type in DashboarderAstType]?: ValidationCheck | ValidationCheck[] }

export class DashboarderValidationRegistry extends ValidationRegistry {
    constructor(services: DashboarderServices) {
        super(services);
        const validator = services.validation.DashboarderValidator;
        const checks: DashboarderChecks = {
            Panel: [validator.checkPanelStartsWithCapital, validator.checkUnusedPanel],
            Page: [
                validator.checkPositionInGrid,
                validator.checkPanelOverlap,
                validator.checkUnusedPage,
                validator.checkRecommendedMinialPanelSize,
            ],
        };
        this.register(checks, validator);
    }
}

export class DashboarderValidator {

    checkPanelStartsWithCapital(panel: Panel, accept: ValidationAcceptor): void {
        if (panel.name) {
            const firstChar = panel.name.substring(0, 1);
            if (firstChar.toUpperCase() !== firstChar) {
                accept('warning', 'Panel name should start with a capital.', { node: panel, property: 'name' });
            }
        }
    }

    checkPositionInGrid(page: Page, accept: ValidationAcceptor): void {
        const MIN_COLUMN = 1;
        const MAX_COLUMN = 13;
        const MIN_ROW = 1;
        const MAX_ROW = 14;

        const positions = page.positions.map(convertToPosition);
        positions.forEach((position, index) => {
            if (position.columnStart === position.columnEnd) {
                accept('error', `Column values cannot be equal.`, { node: page, property: 'positions', index });
            }
            if (position.rowStart === position.rowEnd) {
                accept('error', `Row values cannot be equal.`, { node: page, property: 'positions', index });
            }
            if (position.columnStart < MIN_COLUMN || position.columnEnd > MAX_COLUMN) {
                accept('error', `Column is out of range. (${MIN_COLUMN} to ${MAX_COLUMN})`, { node: page, property: 'positions', index });
            }
            if (position.rowStart < MIN_ROW) {
                accept('error', `Row start is out of range. (minimum: ${MIN_ROW})`, { node: page, property: 'positions', index });
            }
            if (position.rowEnd > MAX_ROW) {
                accept('warning', `Rows should not exceed ${MAX_ROW} to ensure pleasant display.`, { node: page, property: 'positions', index });
            }
        })
    }

    checkPanelOverlap(page: Page, accept: ValidationAcceptor): void {
        const overlaps = new Map<number, Panel[]>();
        const convertedPositions: Position[] = page.positions.map(convertToPosition);
        convertedPositions.forEach((first, firstIndex) => {
            convertedPositions.forEach((second, secondIndex) => {
                if (firstIndex === secondIndex) return;
                if (overlap(first, second)) {
                    const panels: Panel[] = overlaps.get(firstIndex) || [];
                    panels.push(page.panels[secondIndex].ref!);
                    overlaps.set(firstIndex, panels);
                }
            })
        });
        overlaps.forEach((value, index) => {
            accept('warning', `Panel overlaps with ${value.map(panel => panel.name).join(', ')}`, { node: page, property: 'positions', index });
        });
    }

    checkUnusedPanel(node: Panel, accept: ValidationAcceptor) {
        const pages = node.$container.pages;
        for (let page of pages) {
            if (page.panels.find(isReferenced(node))) return;
        }
        accept('warning', 'Panel is unused', { node, property: 'name' });
    }

    checkUnusedPage(node: Page, accept: ValidationAcceptor) {
        const sidebar = node.$container.sidebar;
        if (sidebar.pages.find(isReferenced(node))) return;
        accept('warning', 'Page is unused', { node, property: 'name' });
    }

    checkRecommendedMinialPanelSize(page: Page, accept: ValidationAcceptor) {
        const positions = page.positions.map(convertToPosition);
        positions.forEach((position, index) => {
            let minRow = 1;
            let minColumn = 1;
            const rows = position.rowEnd - position.rowStart;
            const columns = position.columnEnd - position.columnStart;
            const panel = page.panels[index].ref!;
            if (isComparisonPanel(page.panels[index].ref)) { minRow = 2; }
            if (isChartPanel(page.panels[index].ref)) {
                minRow = 2;
                minColumn = 2;
            }
            //@ts-expect-error ast does not move type attribute up to panel
            if (rows < minRow) accept('warning', `Recommended minimal rows for ${panel.type} panel: ${minRow}`, { node: page, property: 'positions', index });
            //@ts-expect-error ast does not move type attribute up to panel
            if (columns < minColumn) accept('warning', `Recommended minimal colunms for ${panel.type} panel: ${minColumn}`, { node: page, property: 'positions', index });
        })
    }
}

interface Position {
    rowStart: number;
    rowEnd: number;
    columnStart: number;
    columnEnd: number;
}

const overlap = (first: Position, second: Position): boolean => {
    if (
        second.columnStart >= first.columnEnd // to right
        || second.columnEnd <= first.columnStart // to left
        || second.rowStart >= first.rowEnd // below
        || second.rowEnd <= first.rowStart // above
    ) return false;
    return true;
}

const convertToPosition = (position: string): Position => {
    const [row, column] = position.split('-');
    const rowElements = row.split('/').map(Number);
    const columnElements = column.split('/').map(Number);

    return { rowStart: _.min(rowElements)!, rowEnd: _.max(rowElements)!, columnStart: _.min(columnElements)!, columnEnd: _.max(columnElements)! };
}


export const isReferenced = <T extends AstNode>(node: T) => (ref: Reference<T>): boolean => (ref.ref === node);
