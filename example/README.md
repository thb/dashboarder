# Example Dashboarder application

This example demonstrates the capabilities of the Dashboarder project.

## Usage

To generate the dashboard execute `yarn dashboarder:generate` or `npm run dashboarder:generate`

To start the example application run `yarn start` or `npm start`

## Additional information

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).
