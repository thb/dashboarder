export const isHttpSource = (str: string): boolean => /^http(s)?:\/\//.test(str);
