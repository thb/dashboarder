import fs from 'fs';
import colors from 'colors';
import { AstNode, LangiumDocument, LangiumServices } from 'langium';
import path from 'path';
import { URI } from 'vscode-uri';
import { ASSET_DIRECTORY, SRC_DIRECTORY } from './constants';
import { FilePathData } from './types';

export async function extractDocument(fileName: string, extensions: string[], services: LangiumServices): Promise<LangiumDocument> {
    if (!extensions.includes(path.extname(fileName))) {
        console.error(colors.yellow(`Please, choose a file with one of these extensions: ${extensions}.`));
        process.exit(1);
    }

    if (!fs.existsSync(fileName)) {
        console.error(colors.red(`File ${fileName} doesn't exist.`));
        process.exit(1);
    }

    const document = services.documents.LangiumDocuments.getOrCreateDocument(URI.file(path.resolve(fileName)));
    const buildResult = await services.documents.DocumentBuilder.build(document);

    const validationErrors = buildResult.diagnostics.filter(e => e.severity === 1);
    if (validationErrors.length > 0) {
        console.error(colors.red('There are validation errors:'));
        for (const validationError of validationErrors) {
            console.error(colors.red(
                `line ${validationError.range.start.line}: ${validationError.message} [${document.textDocument.getText(validationError.range)}]`
            ));
        }
        process.exit(1);
    }

    return document;
}

export async function extractAstNode<T extends AstNode>(fileName: string, extensions: string[], services: LangiumServices): Promise<T> {
    return (await extractDocument(fileName, extensions, services)).parseResult?.value as T;
}

export function extractFilePathData(filePath: string, destination: string | undefined): FilePathData {
    filePath = filePath.replace(/\..*$/, '').replace(/[.-]/g, '');
    const rootDestination = destination ?? path.dirname(filePath);
    return {
        rootDestination,
        srcDestination: path.join(rootDestination, SRC_DIRECTORY),
        assetDestination: path.join(rootDestination, ASSET_DIRECTORY),
        name: path.basename(filePath),
    };
}

const getPackageJson = (data: FilePathData) => {
    const filePath = `${data.rootDestination}/package.json`;
    if (!fs.existsSync(filePath)) return;
    return JSON.parse(fs.readFileSync(filePath, 'utf-8'));
}

export const checkMissingDependencies = (data: FilePathData) => {
    const requiredDependencies = [
        "react-router-dom",
        "chart.js",
        "react-chartjs-2"
    ];
    const packageJson = getPackageJson(data);
    if (!packageJson) {
        console.log(colors.yellow("There is no package.json in destination."));
        console.log(colors.yellow("The generated dashboard does not work without a preexisting application."));
        return;
    }
    const currentDependencies = { ...packageJson.dependencies, ...packageJson.devDependencies };
    const missingDependencies = requiredDependencies.filter(dependency => !Object.keys(currentDependencies).includes(dependency));
    if (missingDependencies.length) {
        console.log(colors.yellow(`You need to add additional dependencies. Execute either of the following:`));
        console.log(colors.yellow(`\tnpm install ${missingDependencies.join(" ")}`));
        console.log(colors.yellow(`\tyarn add ${missingDependencies.join(" ")}`));
    }
}

export const checkGitIgnore = (data: FilePathData) => {
    const recommendedIgnores = [
        SRC_DIRECTORY,
        ASSET_DIRECTORY,
    ];
    const filePath = `${data.rootDestination}/.gitignore`;
    if (!fs.existsSync(filePath)) return;
    const gitIgnore = fs.readFileSync(filePath, "utf-8");
    const currentIgnores = gitIgnore.split("\n").filter((line) => line.length);
    const missingIgnores = recommendedIgnores.filter(ignore => !currentIgnores.includes(ignore));
    if (missingIgnores.length) {
        console.log(colors.blue("You should add the following to your .gitignore:"));
        missingIgnores.map(ignore => console.log(colors.blue(`\t${ignore}`)));
    }
}

export const stripQuotes = (str: string): string => {
    return str.replace(/^"(.+(?="$))"$/, '$1');
};
