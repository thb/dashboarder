import React from "react";
import Panel, { Props as BaseProps } from "./Panel";

interface Props extends BaseProps {}

const TextPanel: React.FC<Props> = (props) => {
  const { source, ...rest } = props;
  return (
    <Panel {...rest} source={source}>
      <span>{source}</span>
    </Panel>
  );
};

export default TextPanel;
