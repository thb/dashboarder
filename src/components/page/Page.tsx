import React from "react";
import styles from "./Page.module.css";

const Page: React.FC = ({ children }) => {
  return (
    <div className={styles.container}>
      {children}
    </div>
  )
}

export default Page;
