import React from 'react';
import ScrumDashboard from './generated/dashboards/ScrumDashboard';

function App() {
  return (<ScrumDashboard />);
}

export default App;
