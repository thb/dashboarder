import React, { ReactNode } from "react";
import { BrowserRouter as Router, Routes } from "react-router-dom";
import styles from "./Dashboard.module.css";

interface Props {
  name: string,
  sidebar?: ReactNode,
}

const Dashboard: React.FC<Props> = ({ sidebar, children }) => {
  return (
    <div className={styles.dashboard}>
      <Router>
        {sidebar}
        <Routes>
          {children}
        </Routes>
      </Router>
    </div>
  )
}

export default Dashboard;
