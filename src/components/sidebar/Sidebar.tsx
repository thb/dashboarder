import styles from './Sidebar.module.css';

const Sidebar: React.FC = ({ children }) => {
    return (
        <div className={styles.sidebar}>
            <div className={styles.sidebarContent}>
                {children}
            </div>
        </div>
    );
};

export default Sidebar;
