import { CSSProperties } from 'react';
import styles from './Panel.module.css';

export interface Props {
  name: string,
  title?: string,
  size: CSSProperties,
  source: string;
}

const Panel: React.FC<Props> = ({ name, title, size, children }) => {
  return (
    <div className={styles.container} style={size}>
      <div className={styles.title}>{title || name}</div>
      <div className={styles.body}>
        {children}
      </div>
    </div>
  );
};

export default Panel;
