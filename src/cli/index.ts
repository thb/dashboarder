import { createSyntaxDiagramsCode, EmbeddedActionsParser } from 'chevrotain';
import colors from 'colors';
import { Command } from 'commander';
import fs from 'fs-extra';
import path from 'path';
import { languageMetaData } from '../language-server/generated/module';
import { Model } from '../language-server/generated/ast';
import { createDashboarderServices } from '../language-server/dashboarder-module';
import { checkGitIgnore, extractAstNode, extractFilePathData, checkMissingDependencies } from './cli-util';
import { generateApplication } from './generator';
import { Category } from './types';

export const generateAction = async (fileName: string, opts: GenerateOptions): Promise<void> => {
    const model = await extractAstNode<Model>(fileName, languageMetaData.fileExtensions, createDashboarderServices());
    const data = extractFilePathData(fileName, opts.destination);
    generateApplication(model, data);
    if (!opts.quiet) {
        console.log(colors.green("Generated dashboard."));
        checkMissingDependencies(data);
        checkGitIgnore(data);
        logUsageReminder(model);
    }
};

export const syntaxAction = async (opts: Pick<GenerateOptions, "destination">): Promise<void> => {
    const services = createDashboarderServices();
    //@ts-expect-error
    const chevrotainParser: EmbeddedActionsParser = services.parser.LangiumParser.wrapper;
    const serializedGrammar = chevrotainParser.getSerializedGastProductions();
    const htmlText = createSyntaxDiagramsCode(serializedGrammar);
    const out = path.join(opts.destination || ".", "dashboarder_syntax.html");
    fs.ensureDirSync(path.dirname(out));
    fs.writeFileSync(out, htmlText);
};

const logUsageReminder = (model: Model) => {
    console.log(colors.blue("Remember to add a call to the dashboard somewhere in the application:"));
    console.log(colors.blue(`\t<${model.dashboard.name}${Category.Dashboard} />`));
}

export type GenerateOptions = {
    destination?: string;
    quiet?: boolean;
}

export default function(): void {
    const program = new Command();

    program
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        .version(require('../../package.json').version);

    program
        .command('generate')
        .argument('<file>', `possible file extensions: ${languageMetaData.fileExtensions.join(', ')}`)
        .option('-d, --destination <dir>', 'directory of application. If omited, will be based on generator file.')
        .option('-q, --quiet', 'do not log additional information.')
        .description('Generates a dashboard inside given react application.')
        .action(generateAction);
    
    program
        .command("syntax")
        .option('-d, --destination <dir>', 'output directory for syntax. Current directory if omited.')
        .description('Generates the syntax diagram for dashboarder as .html file')
        .action(syntaxAction);

    program.parse(process.argv);
}
