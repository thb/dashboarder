import React, { useEffect, useState } from "react";
import Panel, { Props as BaseProps } from "./Panel";
import 'chart.js/auto';
import { Chart } from 'react-chartjs-2';
import { ChartTypeRegistry, ChartDataset, ChartData } from "chart.js";
import styles from "./Panel.module.css";

interface Props extends BaseProps {
  variant: keyof ChartTypeRegistry;
}

const rgbValues: Array<[number, number, number]> = [
  [255, 159, 64], //orange
  [75, 192, 192], // green
  [153, 102, 255], //purple
  [255, 205, 86], //yellow
  [54, 162, 235], // blue
];

const bgColors = rgbValues.map(rgb => `rgba(${rgb.join(",")}, 0.25)`);
const borderColors = rgbValues.map(rgb => `rgb(${rgb.join(",")})`);

const ChartPanel: React.FC<Props> = (props) => {
  const { source, variant, ...rest } = props;
  
  const [ chartData, setChartData ] = useState<ChartData>({
    labels: [],
    datasets: [],
  });

  useEffect(() => {
    fetch(source)
    .then(response => response.text())
    .then(createChartData);
  }, [source]);

  const createChartData = (csvText: string) => {
    const lines = csvText.split("\n").filter((line) => line.length);
    if (!lines.length) return;

    const { columns: labels, hasRowLabels } = getColumns(lines.shift()!);

    let datasets: ChartDataset[] = [];
    lines.forEach((row, index) => {
      const cells = row.split(",");
      let data: (number | null)[] = [];
      let label = (hasRowLabels) ? cells.shift() : "";
      cells.forEach(cell => {
        if (cell === "" || isNaN(Number(cell))) {
          data.push(null);
        } else {
          data.push(Number(cell));
        }
      });
      datasets.push({
        label,
        data,
        ...matchColor(index),
      });
    });
    setChartData({ labels, datasets });
  };

  const getColumns = (line: string): { hasRowLabels: boolean, columns: string[] } => {
    const columns = line.split(",");
    const hasRowLabels = columns[0] === "";
    if (hasRowLabels) columns.shift();
    return {
      hasRowLabels,
      columns,
    };
  }

  const matchColor = (index: number): { backgroundColor: string | string[], borderColor: string | string[] } => {
    const indexedVariants: (keyof ChartTypeRegistry)[] = ["bar", "radar"];
    let isIndexed = (indexedVariants.includes(variant));
    return {
      backgroundColor: isIndexed ? bgColors[index % bgColors.length] : bgColors,
      borderColor: isIndexed ? borderColors[index % bgColors.length] : borderColors,
    };
  };

  return (
    <Panel {...rest} source={source}>
      <Chart className={styles.content} type={variant} data={chartData} />
    </Panel>
  );
};

export default ChartPanel;
