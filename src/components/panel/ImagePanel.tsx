import React from "react";
import Panel, { Props as BaseProps } from "./Panel";
import styles from "./Panel.module.css";

interface Props extends BaseProps {
  alt: string;
}

const ImagePanel: React.FC<Props> = (props) => {
  const { source, alt, ...rest } = props;
  return (
    <Panel {...rest} source={source}>
      <img className={styles.content} src={source} alt={alt} />
    </Panel>
  );
};

export default ImagePanel;
