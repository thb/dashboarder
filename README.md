# Dashboarder

This projects allows to generate dashboards based on a custom DSL.

It is based on [langium](https://langium.org/).

## Usage

### Basics

* Run `yarn langium:generate` or `npm run langium:generate` to generate required sources
* Run `yarn build` or `npm run build` to build the project
* Run `./bin/cli generate <file>` to create a dashboard from a `.dash` file

### As VS Code extension

You can use this project as a VS Code extension after building by pressing `F5`.\
This opens a new instance of VS Code with the extension loaded.

This enables syntax checks and highlighting in `.dash` files

### Example application

As a reference an example application is provided in [./example](./example)

## Syntax

Refer to\
[./documentation/dashboarder_syntax.html](./documentation/dashboarder_syntax.html) or\
[./src/language-server/dashboarder.langium](./src/language-server/dashboarder.langium)

## Current limitations

* Does not generate full application (needs preexisting one)
* Currently supported frameworks: React
* No responsive design

## Development

Refer to [./langium-quickstart.md](./langium-quickstart.md) for a head start.
