import fs from 'fs-extra';
import _ from 'lodash';
import { CompositeGeneratorNode, NL, processGeneratorNode } from 'langium';
import { Dashboard, Model, Panel, Page, Sidebar, isImagePanel, Source, FileSource, isChartPanel, isComparisonPanel } from '../language-server/generated/ast';
import { stripQuotes } from './cli-util';
import path from 'path';
import {
    Category,
    ExtendedBaseGenerationInfo,
    ExtendedBodyGenerationInfo,
    ExtendedFileGenerationInfo,
    ExtendedFooterGenerationInfo,
    ExtendedGenerationInfo,
    ExtendedHeadGenerationInfo,
    FilePathData,
    GenerationInfo,
} from './types';
import { ASSET_DIRECTORY, BASE_DIRECTORY } from './constants';

export const generateApplication = (model: Model, data: FilePathData) => {
    removeGenerated(data);
    copyBase(data);

    generateDashboard(model.dashboard, model.sidebar, model.pages, data);
    generateSidebar(model.sidebar, data);
    model.panels.forEach((panel) => generatePanel(panel, data));
    model.pages.forEach((page) => generatePage(page, data));
}

const removeGenerated = (data: FilePathData) => {
    fs.removeSync(data.srcDestination);
    fs.removeSync(data.assetDestination);
};

const copyBase = (data: FilePathData) => {
    const src = path.join(__dirname, "..", "..", "src/components");
    const dest = path.join(data.srcDestination, BASE_DIRECTORY);
    fs.ensureDirSync(dest);
    fs.copySync(src, dest);
}

const copyAsset = (src: string, data: FilePathData) => {
    const file = path.basename(src);
    const dest = path.join(data.assetDestination);
    fs.ensureDirSync(dest);
    fs.copySync(src, path.join(dest, file));
    return `{process.env.PUBLIC_URL + "/${ASSET_DIRECTORY.replace("public/", "")}/${file}"}`;
};

const generateDashboard = (node: Dashboard, sidebar: Sidebar, pages: Page[], data: FilePathData) => {
    const category = Category.Dashboard;
    const name = node.name;
    const additionalImports = [
        `import { Navigate, Route } from "react-router-dom";`,
    ];
    pages.forEach(
        (page) =>
            additionalImports.push(`import ${page.name}${Category.Page} from "../${Category.Page.toLowerCase()}s/${page.name}${Category.Page}";`)
    );
    const attributes = [
        `name="${name}"`,
    ];

    if (sidebar.pages.length > 1) {
        const sidebarComponent = `${sidebar.name}${Category.Sidebar}`;
        additionalImports.push(`import ${sidebarComponent} from "../sidebars/${sidebarComponent}";`);
        attributes.push(`sidebar={<${sidebarComponent} />}`);
    }

    const content: string[] = [];
    pages.forEach(page => {
        content.push(`<Route path="/${page.name}" element={<${page.name}${Category.Page} />} />`);
    });
    content.push(`<Route path="/" element={<Navigate to="/${pages[0].name}" replace={true} />} />`);

    generateComponent({
        node,
        data,
        name,
        category,
        additionalImports,
        attributes,
        content,
    });
}

const generatePanel = (node: Panel, data: FilePathData) => {
    const category = Category.Panel;
    const name = node.name;
    const props = ['gridRow', 'gridColumn'];
    const attributes = [
        `name="${node.name}"`,
        `size={{${props.join(", ")}}}`,
        handleSource(node.source, data),
        node.title && `title="${node.title}"`,
    ];
    if(isImagePanel(node)) {
        attributes.push(`alt="${node.alt}"`);
    }
    if(isChartPanel(node)) {
        attributes.push(`variant="${node.variant.name}"`);
    }
    if(isComparisonPanel(node)) {
        attributes.push(`progressSource="${node.progressSource.src}"`);
        attributes.push(`totalLabel="${node.totalLabel}"`);
        attributes.push(`remainingLabel="${node.remainingLabel}"`);
    }
    generateComponent({
        node,
        data,
        name,
        category,
        props,
        attributes,
    });
}

const handleSource = (source: Source, data: FilePathData) => {
    const src = stripQuotes(source.src);
    if (source.$type === FileSource) { // type guard is broken
        const path = src.replace('file://', '');
        const assetPath = copyAsset(path, data);
        return `source=${assetPath}`;
    }
    return `source="${src}"`;
};

const generatePage = (node: Page, data: FilePathData) => {
    const category = Category.Page;
    const name = node.name;
    const additionalImports = node.panels.map(
        (panel) =>
            `import ${panel.ref!.name}${Category.Panel} from "../${Category.Panel.toLowerCase()}s/${panel.ref!.name}${Category.Panel}";`
    );
    const content: string[] = [];
    node.panels.forEach((panel, index) => {
        const [row, column] = node.positions[index].split("-");
        content.push(`<${panel.ref!.name}${Category.Panel} gridRow="${row}" gridColumn="${column}" />`);
    });
    generateComponent({
        node,
        data,
        name,
        category,
        additionalImports,
        content,
    });
}

const generateSidebar = (node: Sidebar, data: FilePathData) => {
    const category = Category.Sidebar;
    const name = node.name;
    const additionalImports = [ 
        `import { NavLink } from "react-router-dom";`,
        `import styles from "../${BASE_DIRECTORY}/${category.toLowerCase()}/${category}.module.css";`,
     ];
    const content: string[] = [];
    node.pages.forEach(page => {
        content.push(`<NavLink className={({ isActive }) => \`\${styles.link} \${isActive ? styles.isActive : ""}\`} to="/${page.ref!.name}">`);
        content.push(`\t<span className={styles.linkText}>`);
        content.push(`\t\t${page.ref!.title || page.ref!.name}`);
        content.push(`\t</span>`);
        content.push(`</NavLink>`);
    });
    generateComponent({
        node,
        data,
        name,
        category,
        additionalImports,
        content,
    });
}

const generateComponent = (info: GenerationInfo) => {
    const { node, name, category, attributes, additionalImports } = info;
    const fileNode = new CompositeGeneratorNode();
    const type = node.$type;
    const componentName = `${name}${category}`;
    const extendedInfo: ExtendedGenerationInfo = {
        ...info,
        fileNode,
        type,
        componentName,
        additionalImports: additionalImports || [],
        attributes: attributes || [],
    };
    addHead(extendedInfo);
    addBody(extendedInfo);
    addFooter(extendedInfo);
    writeFile(extendedInfo);
};

const addHead = ({ fileNode, type, componentName, category, additionalImports, props }: ExtendedBaseGenerationInfo & ExtendedHeadGenerationInfo) => {
    fileNode.append('import React from "react";', NL);
    _.uniq(additionalImports || []).forEach(line => fileNode.append(line, NL));
    fileNode.append(`import ${type} from "../${BASE_DIRECTORY}/${category.toLowerCase()}/${type}";`, NL, NL);
    if (props) {
        addPropInterface(fileNode, props);
        fileNode.append(`const ${componentName}: React.FC<Props> = (props) => {`, NL);
        fileNode.append(`\t const { ${props.join(", ")} } = props;`, NL);
    } else {
        fileNode.append(`const ${componentName}: React.FC = () => {`, NL);
    }
}

const addPropInterface = (fileNode: CompositeGeneratorNode, props: string[]) => {
    fileNode.append('interface Props {', NL);
    props.forEach((prop) => fileNode.append(`\t${prop}: any;`, NL));
    fileNode.append('}', NL, NL);
};

const addBody = ({ fileNode, type, attributes, content }: ExtendedBaseGenerationInfo & ExtendedBodyGenerationInfo) => {
    if(content) {
        fileNode.append(`\treturn (`, NL);
        fileNode.append(`\t\t<${type} ${attributes.join(" ")}>`, NL);
        content.forEach(line => fileNode.append(`\t\t\t${line}`, NL));
        fileNode.append(`\t\t</${type}>`, NL);
        fileNode.append(`\t);`, NL);
    } else {
        fileNode.append(`\treturn <${type} ${attributes.join(" ")} />;`, NL);
    }
};

const addFooter = ({ fileNode, componentName }: ExtendedBaseGenerationInfo & ExtendedFooterGenerationInfo) => {
    fileNode.append(`}`, NL);
    fileNode.append(`export default ${componentName};`, NL);
};

const writeFile = ({ fileNode, data, category, name, extension, omitCategory }: ExtendedBaseGenerationInfo & ExtendedFileGenerationInfo) => {
    const categoryDirectory = path.join(data.srcDestination, `${category.toLowerCase()}s`);
    const pathName = `${path.join(categoryDirectory, `${name}${!omitCategory ? category : ""}`)}.${extension || 'tsx'}`;
    fs.ensureDirSync(categoryDirectory);
    fs.writeFileSync(pathName, processGeneratorNode(fileNode));
}
