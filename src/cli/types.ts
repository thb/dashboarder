import { AstNode, CompositeGeneratorNode } from "langium";

export interface FilePathData {
  rootDestination: string,
  srcDestination: string,
  assetDestination: string,
  name: string
}

export enum Category {
  Dashboard = "Dashboard",
  Page = "Page",
  Sidebar = "Sidebar",
  Panel = "Panel",
}

export interface BaseGenerationInfo {
  node: AstNode;
  name: string;
  category: Category;
}

export interface HeadGenerationInfo {
  additionalImports?: string[];
  props?: string[];
}

export interface BodyGenerationInfo {
  attributes?: string[];
  content?: string[];
}

export interface FooterGenerationInfo { }

export interface FileGenerationInfo {
  data: FilePathData;
  extension?: string;
  omitCategory?: boolean;
}

export type GenerationInfo =
  & BaseGenerationInfo
  & HeadGenerationInfo
  & BodyGenerationInfo
  & FooterGenerationInfo
  & FileGenerationInfo;


export interface ExtendedBaseGenerationInfo extends BaseGenerationInfo {
  fileNode: CompositeGeneratorNode;
  type: string;
  componentName: string;
}

export interface ExtendedHeadGenerationInfo extends HeadGenerationInfo {
  additionalImports: string[];
}

export interface ExtendedBodyGenerationInfo extends BodyGenerationInfo {
  attributes: string[];
}

export interface ExtendedFooterGenerationInfo extends FooterGenerationInfo { }

export interface ExtendedFileGenerationInfo extends FileGenerationInfo { }

export type ExtendedGenerationInfo =
  & GenerationInfo
  & ExtendedBaseGenerationInfo
  & ExtendedHeadGenerationInfo
  & ExtendedBodyGenerationInfo
  & ExtendedFooterGenerationInfo
  & ExtendedFileGenerationInfo;
