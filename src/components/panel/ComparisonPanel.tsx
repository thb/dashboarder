import React, { Dispatch, SetStateAction, useEffect, useState } from "react";
import Panel, { Props as BaseProps } from "./Panel";
import { isHttpSource } from "../helpers";
import baseStyles from "./Panel.module.css";
import styles from "./ComparisonPanel.module.css";

interface Props extends BaseProps {
  progressSource: string;
  totalLabel: string;
  remainingLabel: string;
}

const ComparisonPanel: React.FC<Props> = (props) => {
  const { source, progressSource, totalLabel, remainingLabel, ...rest } = props;
  const [total, setTotal] = useState(0);
  const [progress, setProgress] = useState(0);

  useEffect(() => {
    setValue(source, setTotal);
    setValue(progressSource, setProgress);
  }, [source, progressSource]);

  const setValue = (source: string, setter: Dispatch<SetStateAction<number>>) => {
    if(isHttpSource(source)) {
      fetch(source)
      .then(response => response.text())
      .then(total => setter(Number(total)));
    } else {
      setter(Number(source));
    }
  };

  return (
    <Panel {...rest} source={source}>
      <div className={baseStyles.content}>
        <div className={styles.container}>
          <div className={styles.progress}>{progress}</div>
          <div className={`${styles.label} ${styles.total} ${styles.totalLabel}`}>{totalLabel}</div>
          <div className={`${styles.label} ${styles.remaining} ${styles.remainingLabel}`}>{remainingLabel}</div>
          <div className={`${styles.total} ${styles.totalValue}`}>{total}</div>
          <div className={` ${styles.remaining} ${styles.remainingValue}`}>{total-progress}</div>
        </div>
      </div>
    </Panel>
  );
};

export default ComparisonPanel;
